bl_info = {
    "name": "Armature_Rigging",
    "description": "Create an Armature with Markers",
    "author": "Jasmin R (jar_25)",
    "version": (0, 9),
    "blender": (2, 79, 0),
    "location": "View3D > Tools > Armature",
    "support": "TESTING",
    "category": "Rigging"
    }

import bpy
from math import pi
import mathutils


class addEmpties(bpy.types.Operator):
    bl_idname ="myops.add_empties"
    bl_label="Add empties"
    bl_options={"REGISTER", "UNDO"}
    
    def execute(self,context):
        scene = context.scene
        objects = bpy.data.objects
        e_s = objects.new("steiss", None)
        e_h = objects.new("hals", None)
        e_k1 = objects.new("kopf_anfang", None)
        e_k2 = objects.new("kopf_ende",None)
        e_sr = objects.new("schulter_r", None)
        e_sl = objects.new("schulter_l", None)
        e_er = objects.new("ellenbogen_r", None)
        e_el = objects.new("ellenbogen_l", None)
        e_hr = objects.new("handgelenk_r", None)
        e_hl = objects.new("handgelenk_l", None)
        e_fsr = objects.new("fingerspitze_r", None)
        e_fsl = objects.new("fingerspitze_l", None)
        e_hur = objects.new("huefte_r", None)
        e_hul = objects.new("huefte_l", None)
        e_kr = objects.new("knie_r", None)
        e_kl = objects.new("knie_l", None)
        e_fr = objects.new("fussgelenk_r", None)
        e_fl = objects.new("fussgelenk_l", None)
        e_zr = objects.new("zehenspitze_r", None)
        e_zl = objects.new("zehenspitze_l", None)
        
        e_a_ik_r= objects.new("arm_ik_r", None)
        e_a_ik_l= objects.new("arm_ik_l", None)
        e_e_ik_r= objects.new("ellenbogen_ik_r", None)
        e_e_ik_l= objects.new("ellenbogen_ik_l", None)
        e_f_ik_r= objects.new("fuss_ik_r", None)
        e_f_ik_l= objects.new("fuss_ik_l", None)
        e_k_ik_r= objects.new("knie_ik_r", None)
        e_k_ik_l= objects.new("knie_ik_l", None)
        
        e_controller= objects.new("controller", None)
        
        
        e_s.location = (0, 0, 1)
        e_h.location = (0, 0, 3)
        e_k1.location = (0, 0, 3.5)
        e_k2.location = (0, 0, 4)
        e_sr.location = (0, -1, 3)
        e_sl.location = (0, 1, 3)
        
        e_er.location = (-0.2, -1.5, 2.5)
        e_e_ik_r.location = (-0.5, -1.5, 2.5)
        
        e_el.location = (-0.2, 1.5, 2.5)
        e_e_ik_l.location = (-0.5, 1.5, 2.5)
        
        
        e_hr.location = (0, -2, 2)
        e_a_ik_r.location = (-0.5, -2, 2)
        
        e_hl.location = (0, 2, 2)
        e_a_ik_l.location = (-0.5, 2, 2)
        
        e_fsl.location = (0, 2.5, 1.5)
        e_fsr.location = (0, -2.5, 1.5)
        
        e_hur.location = (0, -1, 0.5)
        e_hul.location = (0, 1, 0.5)
        
        e_kr.location = (0.1, -1, 0)
        e_k_ik_r.location = (0.5, -1, 0)
        e_kl.location = (0.1, 1, 0)
        e_k_ik_l.location = (0.5, 1, 0)
        
        e_fr.location = (0, -1, -1)
        e_f_ik_r.location = (-0.5, -1, -1)
        e_fl.location = (0, 1, -1)
        e_f_ik_l.location = (-0.5, 1, -1)
        
        e_zr.location = (0.5, -1, -1)
        e_zl.location = (0.5, 1, -1)
        
        e_controller.location = (0, 0, -1)
        
        bone_list=[e_s, e_h, e_k2, e_k1, e_sl, e_sr, e_el, e_er, e_hr, e_hl, e_fsr, e_fsl, e_hul, e_hur, e_kr, e_kl, e_fr, e_fl, e_zr, e_zl, e_e_ik_l, e_e_ik_r, e_a_ik_r, e_a_ik_l, e_k_ik_r, e_k_ik_l, e_f_ik_r, e_f_ik_l, e_controller] 
        
        for i in bone_list:
            i.empty_draw_type='SPHERE'
            i.empty_draw_size = 0.06
            scene.objects.link(i)
         
        scene.update()
        return {"FINISHED"}


class panel1(bpy.types.Panel):
    bl_idname = "panel.panel1"
    bl_label = "Automatic Armature"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_category = "Tools"
    bl_category = "Automatic Armature"
 
    def draw(self, context):
        self.layout.operator("myops.add_empties", icon='EMPTY_DATA', text="Create Empties") 
        self.layout.operator("myops.add_myarmature", icon='ARMATURE_DATA', text="Create Armature")
        self.layout.operator("myops.add_constraints", icon ='MESH_DATA', text="Create Constraints")
        self.layout.operator("myops.clear_pose", icon ='POSE_HLT', text="Reset Pose")
        self.layout.operator("myops.join_armature", icon ='CONSTRAINT_DATA', text="Join Armature")
        self.layout.operator("myops.clear_all", icon ='CANCEL', text="Clear Armature")
        
        


class addArmature(bpy.types.Operator):
    bl_idname="myops.add_myarmature"
    bl_label="Add new Armature"
    bl_options={"REGISTER", "UNDO"}
    
    def execute(self, context):
        bpy.ops.object.armature_add()
        bpy.context.object.show_x_ray = True

        ob = bpy.context.scene.objects.active
        ob.name= "Armature"
        arm = ob.data
        
        
        steiss = bpy.data.objects['steiss'].location
        hals = bpy.data.objects['hals'].location
        schulter_l = bpy.data.objects['schulter_l'].location
        handgelenk_l = bpy.data.objects['handgelenk_l'].location
        fingerspitze_l = bpy.data.objects['fingerspitze_l'].location
        ellenbogen_l = bpy.data.objects['ellenbogen_l'].location
        kopf1 = bpy.data.objects['kopf_anfang'].location
        kopf2 = bpy.data.objects['kopf_ende'].location
        schulter_r = bpy.data.objects['schulter_r'].location
        ellenbogen_r = bpy.data.objects['ellenbogen_r'].location
        handgelenk_r = bpy.data.objects['handgelenk_r'].location
        fingerspitze_r = bpy.data.objects['fingerspitze_r'].location
        huefte_l = bpy.data.objects['huefte_l'].location
        knie_l = bpy.data.objects['knie_l'].location
        fussgelenk_l = bpy.data.objects['fussgelenk_l'].location
        zehenspitze_l = bpy.data.objects['zehenspitze_l'].location
        huefte_r = bpy.data.objects['huefte_r'].location
        knie_r = bpy.data.objects['knie_r'].location
        fussgelenk_r = bpy.data.objects['fussgelenk_r'].location
        zehenspitze_r = bpy.data.objects['zehenspitze_r'].location
        ellenbogen_ik_r = bpy.data.objects['ellenbogen_ik_r'].location
        ellenbogen_ik_l = bpy.data.objects['ellenbogen_ik_l'].location
        arm_ik_r = bpy.data.objects['arm_ik_r'].location
        arm_ik_l = bpy.data.objects['arm_ik_l'].location
        knie_ik_l = bpy.data.objects['knie_ik_l'].location
        knie_ik_r = bpy.data.objects['knie_ik_r'].location
        fuss_ik_r = bpy.data.objects['fuss_ik_r'].location
        fuss_ik_l = bpy.data.objects['fuss_ik_l'].location
        controller = bpy.data.objects['controller'].location




 
        bpy.ops.object.mode_set(mode='EDIT')
        for i in range(4):
            bpy.ops.armature.extrude()
            bpy.ops.transform.translate(value=(0.0, 0.0, 1.0))

        bpy.ops.armature.bone_primitive_add(name = "Schulter_L")
        for i in range(3):
            bpy.ops.armature.extrude()
            bpy.ops.transform.translate(value=(0.0, 0.0, 1.0))
        bpy.ops.armature.bone_primitive_add(name = "Schulter_R")
        for i in range(3):
            bpy.ops.armature.extrude()
            bpy.ops.transform.translate(value=(0.0, 0.0, 1.0))

        bpy.ops.armature.bone_primitive_add(name = "Huefte_L")
        for i in range(3):
            bpy.ops.armature.extrude()
            bpy.ops.transform.translate(value=(0.0, 0.0, 1.0))
    
    
        bpy.ops.armature.bone_primitive_add(name = "Huefte_R")
        for i in range(3):
           bpy.ops.armature.extrude()
           bpy.ops.transform.translate(value=(0.0, 0.0, 1.0))
           
        bpy.ops.armature.bone_primitive_add(name = "Ellenbogen_IK_R")
        bpy.ops.transform.translate(value=(0.0, 0.0, 1.0))  
        
        bpy.ops.armature.bone_primitive_add(name = "Ellenbogen_IK_L")
        bpy.ops.transform.translate(value=(0.0, 0.0, 1.0))
        
        bpy.ops.armature.bone_primitive_add(name = "Arm_IK_R")
        bpy.ops.transform.translate(value=(0.0, 0.0, 1.0))
        
        bpy.ops.armature.bone_primitive_add(name = "Arm_IK_L")
        bpy.ops.transform.translate(value=(0.0, 0.0, 1.0))
        
        bpy.ops.armature.bone_primitive_add(name = "Knie_IK_R")
        bpy.ops.transform.translate(value=(0.0, 0.0, 1.0))
        
        bpy.ops.armature.bone_primitive_add(name = "Knie_IK_L")
        bpy.ops.transform.translate(value=(0.0, 0.0, 1.0))
        
        bpy.ops.armature.bone_primitive_add(name = "Fuss_IK_R")
        bpy.ops.transform.translate(value=(0.0, 0.0, 1.0))
        
        bpy.ops.armature.bone_primitive_add(name = "Fuss_IK_L")
        bpy.ops.transform.translate(value=(0.0, 0.0, 1.0))
        
        bpy.ops.armature.bone_primitive_add(name = "Controller")
        bpy.ops.transform.translate(value=(0.0, 0.0, 1.0))
        
        obj = bpy.data.objects['Armature']

        bpy.context.scene.objects.active = obj

        
        for i in range(len(arm.edit_bones)):
            eb = arm.edit_bones[i]
            if i == 0:
                eb.name= "Steiß"
                eb.head = obj.matrix_world.inverted()*steiss
                eb.tail[0] = eb.head[0]
                eb.tail[1] = eb.head[1]
                eb.tail[2] = eb.head[2] +0.5      
                
            if i >0 and i<3:
                eb.name= "Ruecken_" + str(i)
                eb.tail[0] = eb.head[0]
                eb.tail[1] = eb.head[1]
                eb.tail[2] = eb.head[2] +0.5
    
            if i == 3:
                eb.name = "hals"
                eb.head = obj.matrix_world.inverted()*hals
                eb.tail = obj.matrix_world.inverted()*kopf1
   
            if i == 4:
                eb.name = "Kopf"
                eb.tail = obj.matrix_world.inverted()*kopf2
                eb.head = obj.matrix_world.inverted()*kopf1
   
            if i == 5:
                eb.tail = obj.matrix_world.inverted()*schulter_l
                eb.head[0] = eb.tail[0] 
                eb.head[1] = eb.tail[1] -0.5
                eb.head[2] = eb.tail[2] 
    
            if i ==6:
                eb.name="Oberarm_l"
                eb.tail = obj.matrix_world.inverted()*ellenbogen_l
            if i == 7:
                eb.name = "Unterarm_l"
                eb.tail = obj.matrix_world.inverted()*handgelenk_l
            if i == 8:
                eb.name ="Hand_l"
                eb.tail = obj.matrix_world.inverted()*fingerspitze_l
                
            if i ==9:
                eb.tail = obj.matrix_world.inverted()*schulter_r
                eb.head[0] = eb.tail[0] 
                eb.head[1] = eb.tail[1] +0.5
                eb.head[2] = eb.tail[2] 
        
            if i == 10:
                eb.name="Oberarm_r"
                eb.tail = obj.matrix_world.inverted()*ellenbogen_r
            if i == 11:
                eb.name="Unterarm_r"
                eb.tail = obj.matrix_world.inverted()*handgelenk_r
            if i ==12:
                eb.name = "Hand_r"
                eb.tail = obj.matrix_world.inverted()*fingerspitze_r
            if i == 13:
                eb.name = "Hüfte_l"
                eb.tail = obj.matrix_world.inverted()*huefte_l
                eb.head[0] = eb.tail[0] 
                eb.head[1] = eb.tail[1] -0.5
                eb.head[2] = eb.tail[2] +0.5
            if i ==14:
                eb.name="Oberschenkel_l"
                eb.tail = obj.matrix_world.inverted()*knie_l
            if i == 15:
                eb.name="Unterschenkel_l"
                eb.tail= obj.matrix_world.inverted()*fussgelenk_l
            if i ==16:
                eb.name="Fuss_l"
                eb.tail= obj.matrix_world.inverted()*zehenspitze_l
            if i == 17:
                eb.name="Hüfte_R"
                eb.tail = obj.matrix_world.inverted()*huefte_r
                eb.head[0] = eb.tail[0] 
                eb.head[1] = eb.tail[1] +0.5
                eb.head[2] = eb.tail[2] +0.5
            if i ==18:
                eb.name="Oberschenkel_r"
                eb.tail = obj.matrix_world.inverted()*knie_r
            if i == 19:
                eb.name="Unterschenkel_r"
                eb.tail =obj.matrix_world.inverted()*fussgelenk_r
            if i ==20:
                eb.name ="Fuss_r"
                eb.tail = obj.matrix_world.inverted()*zehenspitze_r
            
            if i ==21:
                
                eb.head = obj.matrix_world.inverted()*ellenbogen_r
                eb.tail = obj.matrix_world.inverted()*ellenbogen_ik_r
                eb.use_deform=False
            if i ==22:
                
                eb.head = obj.matrix_world.inverted()*ellenbogen_l
                eb.tail = obj.matrix_world.inverted()*ellenbogen_ik_l
                eb.use_deform=False
                
            if i ==23:
                
                eb.head = obj.matrix_world.inverted()*handgelenk_r
                eb.tail = obj.matrix_world.inverted()*arm_ik_r
                eb.use_deform=False
                
            if i ==24:
                
                eb.head = obj.matrix_world.inverted()*handgelenk_l
                eb.tail = obj.matrix_world.inverted()*arm_ik_l
                eb.use_deform=False
                
            if i ==25:
                
                eb.head = obj.matrix_world.inverted()*knie_r
                eb.tail = obj.matrix_world.inverted()*knie_ik_r
                eb.use_deform=False
                
            if i ==26:
                
                eb.head = obj.matrix_world.inverted()*knie_l
                eb.tail = obj.matrix_world.inverted()*knie_ik_l
                eb.use_deform=False
                
            if i ==27:
                
                eb.head = obj.matrix_world.inverted()*fussgelenk_r
                eb.tail = obj.matrix_world.inverted()*fuss_ik_r
                eb.use_deform=False
                
            
            if i ==28:
                
                eb.head = obj.matrix_world.inverted()*fussgelenk_l
                eb.tail = obj.matrix_world.inverted()*fuss_ik_l
                eb.use_deform=False
            
            if i == 29:
                eb.head = obj.matrix_world.inverted()*controller
                eb.tail[0] = eb.head[0]-0.5
                eb.tail[1]= eb.head[1]
                eb.tail[2]= eb.head[2]
                
                eb.use_deform=False
      
        return {"FINISHED"}
    
class addConstraints(bpy.types.Operator):
    bl_idname ="myops.add_constraints"
    bl_label="Add Constraints"
    bl_options={"REGISTER", "UNDO"}
    
    def execute(self,context):
        ob = bpy.context.scene.objects.active
        ob.name= "Armature"
        arm = ob.data
        
        ##----------------Keep Offsets----------------#
        #Fuss R
        bpy.ops.armature.select_all(action='TOGGLE')
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.context.object.data.bones['Fuss_r'].select =True
        bpy.context.object.data.bones['Fuss_IK_R'].select =True
        bpy.context.object.data.bones.active=bpy.context.object.data.bones['Fuss_IK_R']
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.armature.parent_set(type='OFFSET')
        
        
        #Fuss L
        bpy.ops.armature.select_all(action='TOGGLE')
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.context.object.data.bones['Hand_l'].select =True
        bpy.context.object.data.bones['Arm_IK_L'].select =True
        bpy.context.object.data.bones.active=bpy.context.object.data.bones['Arm_IK_L']
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.armature.parent_set(type='OFFSET')
        
        
        #Arm R
        bpy.ops.armature.select_all(action='TOGGLE')
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.context.object.data.bones['Hand_r'].select =True
        bpy.context.object.data.bones['Arm_IK_R'].select =True
        bpy.context.object.data.bones.active=bpy.context.object.data.bones['Arm_IK_R']
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.armature.parent_set(type='OFFSET')
        
        
        #Arm L
        bpy.ops.armature.select_all(action='TOGGLE')
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.context.object.data.bones['Fuss_l'].select =True
        bpy.context.object.data.bones['Fuss_IK_L'].select =True
        bpy.context.object.data.bones.active=bpy.context.object.data.bones['Fuss_IK_L']
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.armature.parent_set(type='OFFSET')
        
        
        #Hüfte zu Steiss
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.armature.select_all(action='TOGGLE')
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.context.object.data.bones['Hüfte_l'].select =True
        bpy.context.object.data.bones['Hüfte_R'].select =True
        bpy.context.object.data.bones['Steiß'].select =True
        bpy.context.object.data.bones.active=bpy.context.object.data.bones['Steiß']
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.armature.parent_set(type='OFFSET')
        
        
        #Schultern zu Rücken
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.armature.select_all(action='TOGGLE')
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.context.object.data.bones['Schulter_R'].select =True
        bpy.context.object.data.bones['Schulter_L'].select =True
        bpy.context.object.data.bones['Ruecken_2'].select =True
        bpy.context.object.data.bones.active=bpy.context.object.data.bones['Ruecken_2']
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.armature.parent_set(type='OFFSET')
        
        #Controller
        
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.armature.select_all(action='TOGGLE')
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.context.object.data.bones['Arm_IK_R'].select =True
        bpy.context.object.data.bones['Arm_IK_L'].select =True
        bpy.context.object.data.bones['Fuss_IK_R'].select =True
        bpy.context.object.data.bones['Fuss_IK_L'].select =True
        bpy.context.object.data.bones['Knie_IK_R'].select =True
        bpy.context.object.data.bones['Knie_IK_L'].select =True
        bpy.context.object.data.bones['Ellenbogen_IK_R'].select =True
        bpy.context.object.data.bones['Ellenbogen_IK_L'].select =True
        bpy.context.object.data.bones['Steiß'].select =True
        bpy.context.object.data.bones.active=bpy.context.object.data.bones['Controller']
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.armature.parent_set(type='OFFSET')
        
        
        
       #------------------IK-------------------#
       #IK Rechter Fuss
        bpy.ops.object.mode_set(mode='POSE')
        bpy.ops.pose.select_all(action='TOGGLE')
        #bpy.context.object.data.bones['Unterschenkel_r'].select =True
        bpy.context.object.data.bones.active=bpy.context.object.data.bones['Unterschenkel_r']
        bpy.ops.pose.constraint_add(type='IK')
        bpy.context.object.pose.bones["Unterschenkel_r"].constraints["IK"].target=bpy.data.objects["Armature"]
        bpy.context.object.pose.bones["Unterschenkel_r"].constraints["IK"].subtarget = "Fuss_IK_R"
        bpy.context.object.pose.bones["Unterschenkel_r"].constraints["IK"].pole_target = bpy.data.objects["Armature"]
        bpy.context.object.pose.bones["Unterschenkel_r"].constraints["IK"].pole_subtarget = "Knie_IK_R"
        bpy.context.object.pose.bones["Unterschenkel_r"].constraints["IK"].chain_count = 2

        
        #IK Linker Fuss
        
        bpy.ops.pose.select_all(action='TOGGLE')
        bpy.context.object.data.bones.active=bpy.context.object.data.bones['Unterschenkel_l']
        bpy.ops.pose.constraint_add(type='IK')
        bpy.context.object.pose.bones["Unterschenkel_l"].constraints["IK"].target=bpy.data.objects["Armature"]
        bpy.context.object.pose.bones["Unterschenkel_l"].constraints["IK"].subtarget = "Fuss_IK_L"
        bpy.context.object.pose.bones["Unterschenkel_l"].constraints["IK"].pole_target = bpy.data.objects["Armature"]
        bpy.context.object.pose.bones["Unterschenkel_l"].constraints["IK"].pole_subtarget = "Knie_IK_L"
        bpy.context.object.pose.bones["Unterschenkel_l"].constraints["IK"].chain_count = 2
        
        
        #IK Rechter Arm
        bpy.ops.pose.select_all(action='TOGGLE')
        bpy.context.object.data.bones.active=bpy.context.object.data.bones['Unterarm_l']
        bpy.ops.pose.constraint_add(type='IK')
        bpy.context.object.pose.bones["Unterarm_l"].constraints["IK"].target = bpy.data.objects["Armature"]
        bpy.context.object.pose.bones["Unterarm_l"].constraints["IK"].subtarget = "Arm_IK_L"
        bpy.context.object.pose.bones["Unterarm_l"].constraints["IK"].pole_target = bpy.data.objects["Armature"]
        bpy.context.object.pose.bones["Unterarm_l"].constraints["IK"].pole_subtarget = "Ellenbogen_IK_L"
        bpy.context.object.pose.bones["Unterarm_l"].constraints["IK"].chain_count = 2
        bpy.context.object.pose.bones["Unterarm_l"].constraints["IK"].pole_angle = 3.14159

        
        
        #IK Linker Arm
         #IK Rechter Arm
        bpy.ops.pose.select_all(action='TOGGLE')
        bpy.context.object.data.bones.active=bpy.context.object.data.bones['Unterarm_r']
        bpy.ops.pose.constraint_add(type='IK')
        bpy.context.object.pose.bones["Unterarm_r"].constraints["IK"].target = bpy.data.objects["Armature"]
        bpy.context.object.pose.bones["Unterarm_r"].constraints["IK"].subtarget = "Arm_IK_R"
        bpy.context.object.pose.bones["Unterarm_r"].constraints["IK"].pole_target = bpy.data.objects["Armature"]
        bpy.context.object.pose.bones["Unterarm_r"].constraints["IK"].pole_subtarget = "Ellenbogen_IK_R"
        bpy.context.object.pose.bones["Unterarm_r"].constraints["IK"].pole_angle = 3.14159

        bpy.context.object.pose.bones["Unterarm_r"].constraints["IK"].chain_count = 2

        return{"FINISHED"}
    
class ClearAll(bpy.types.Operator):
    bl_idname ="myops.clear_all"
    bl_label="Clear All"
    bl_options={"REGISTER", "UNDO"}
    
    def execute(self,context):
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.delete()
        return{"FINISHED"}
    

class ClearPose(bpy.types.Operator):
    bl_idname ="myops.clear_pose"
    bl_label="Clear Pose"
    bl_options={"REGISTER", "UNDO"}
    
    def execute(self,context):
        bpy.ops.object.mode_set(mode='POSE')
        bpy.ops.pose.select_all(action='TOGGLE')
        bpy.ops.pose.select_all(action='TOGGLE')
        bpy.ops.pose.loc_clear()
        bpy.ops.pose.rot_clear()

        return{"FINISHED"}
    
    
class JoinArmature(bpy.types.Operator):
    bl_idname ="myops.join_armature"
    bl_label="Clear Pose"
    bl_options={"REGISTER", "UNDO"}
    
    def execute(self,context):
        
        bpy.data.objects['Armature'].select= True   
       # bpy.data.objects['Armature'].select= True
        bpy.context.scene.objects.active = bpy.data.objects['Armature']
        #bpy.data.objects['Armature'].select= True
        
        
        bpy.ops.object.parent_set(type='ARMATURE_AUTO')

       

        return{"FINISHED"}

def register() :
    bpy.utils.register_class(addArmature)
    bpy.utils.register_class(addEmpties)
    bpy.utils.register_class(addConstraints)
    bpy.utils.register_class(ClearAll)
    bpy.utils.register_class(ClearPose)
    bpy.utils.register_class(JoinArmature)
    bpy.utils.register_class(panel1)
   
 
def unregister() :
    bpy.utils.unregister_class(addArmature)
    bpy.utils.unregister_class(addEmpties)
    bpy.utils.unregister_class(addConstraints)
    bpy.utils.unregister_class(ClearAll)
    bpy.utils.unregister_class(ClearPose)
    bpy.utils.unregister_class(JoinArmature)
    bpy.utils.unregister_class(panel1)
   
 
if __name__ == "__main__" :
    register()